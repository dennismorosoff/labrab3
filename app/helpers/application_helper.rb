module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_caption(full_title, page_title)
    return "#{full_title} | #{page_title}"
    return full_title if page_title.empty?
    return page_title if full_title.empty?
  end
end
